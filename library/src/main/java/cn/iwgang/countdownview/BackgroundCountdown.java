package cn.iwgang.countdownview;


import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;


class BackgroundCountdown extends BaseCountdown {
    private static final float DEFAULT_TIME_BG_DIVISION_LINE_SIZE = 0.5f; // dp
    private static final float DEFAULT_TIME_BG_BORDER_SIZE = 1f; // dp

    private boolean isDrawBg;
    private boolean isShowTimeBgDivisionLine;
    private Color mTimeBgDivisionLineColor=Color.WHITE;
    private float mTimeBgDivisionLineSize;
    private float mTimeBgRadius;
    private float mTimeBgSize;
    private Color mTimeBgColor=Color.WHITE;
    private Paint mTimeBgPaint;
    Paint mTimeBgBorderPaint;
    Paint mTimeBgDivisionLinePaint;

    private float mDefSetTimeBgSize;
    private float mDayTimeBgWidth;
    private RectFloat mDayBgRectF;
    RectFloat mHourBgRectF;
    RectFloat mMinuteBgRectF;
    RectFloat mSecondBgRectF;
    RectFloat mMillisecondBgRectF;

    private RectFloat mDayBgBorderRectF;
    RectFloat mHourBgBorderRectF;
    RectFloat mMinuteBgBorderRectF;
    RectFloat mSecondBgBorderRectF;
    RectFloat mMillisecondBgBorderRectF;

    private float mTimeBgDivisionLineYPos;
    private float mTimeTextBaseY;
    private boolean isShowTimeBgBorder;
    private float mTimeBgBorderSize;
    private float mTimeBgBorderRadius;
    private Color mTimeBgBorderColor=Color.WHITE;
    private boolean hasTimeBgColor;

    @Override
    public void initStyleAttr(Component component,Context context, AttrSet attrs) {
        super.initStyleAttr(component,context, attrs);
        boolean presentTimeBgColor = attrs.getAttr("timeBgColor").isPresent();
        if (presentTimeBgColor) {
            mTimeBgColor = attrs.getAttr("timeBgColor").get().getColorValue();
            hasTimeBgColor = true;
        } else {
            hasTimeBgColor = false;
        }
        boolean presentTimeBgRadius = attrs.getAttr("timeBgRadius").isPresent();
        if (presentTimeBgRadius) {
            mTimeBgRadius = attrs.getAttr("timeBgRadius").get().getDimensionValue();
        }

        boolean presentIsShowTimeBgDivisionLine = attrs.getAttr("isShowTimeBgDivisionLine").isPresent();
        if (presentIsShowTimeBgDivisionLine) {
            isShowTimeBgDivisionLine = attrs.getAttr("isShowTimeBgDivisionLine").get().getBoolValue();
        }

        boolean presentTimeBgDivisionLineColor= attrs.getAttr("timeBgDivisionLineColor").isPresent();
        if (presentTimeBgDivisionLineColor) {
            mTimeBgDivisionLineColor = attrs.getAttr("timeBgDivisionLineColor").get().getColorValue();
        }

        boolean presentTimeBgDivisionLineSize = attrs.getAttr("timeBgDivisionLineSize").isPresent();
        if (presentTimeBgDivisionLineSize) {
            mTimeBgDivisionLineSize = attrs.getAttr("timeBgDivisionLineSize").get().getDimensionValue();
        }

        boolean presentTimeBgSize = attrs.getAttr("timeBgSize").isPresent();
        if (presentTimeBgSize) {
            mTimeBgSize = attrs.getAttr("timeBgSize").get().getDimensionValue();
        }
        mDefSetTimeBgSize = mTimeBgSize;

        boolean presentTimeBgBorderSize = attrs.getAttr("timeBgBorderSize").isPresent();
        if (presentTimeBgBorderSize) {
            mTimeBgBorderSize = attrs.getAttr("timeBgBorderSize").get().getDimensionValue();
        }
        boolean presentTimeBgBorderRadius = attrs.getAttr("timeBgBorderRadius").isPresent();
        if (presentTimeBgBorderRadius) {
            mTimeBgBorderRadius = attrs.getAttr("timeBgBorderRadius").get().getDimensionValue();
        }
        boolean presentTimeBgBorderColor = attrs.getAttr("timeBgBorderColor").isPresent();
        if (presentTimeBgBorderColor) {
            mTimeBgBorderColor = attrs.getAttr("timeBgBorderColor").get().getColorValue();
        }

        boolean presentIsShowTimeBgBorder = attrs.getAttr("isShowTimeBgBorder").isPresent();
        if (presentIsShowTimeBgBorder) {
            isShowTimeBgBorder = attrs.getAttr("isShowTimeBgBorder").get().getBoolValue();
        }
        isDrawBg=hasTimeBgColor||!isShowTimeBgBorder;
  }

    @Override
    protected void initPaint() {
        super.initPaint();
        // time background
        mTimeBgPaint = new Paint();
        mTimeBgPaint.setStyle(Paint.Style.FILL_STYLE);
        mTimeBgPaint.setAntiAlias(true);
        mTimeBgPaint.setColor(mTimeBgColor);

        // time background border
        if (isShowTimeBgBorder) {
            initTimeBgBorderPaint();
        }

        // time background division line
        if (isShowTimeBgDivisionLine) {
            initTimeTextBgDivisionLinePaint();
        }
    }

    private void initTimeBgBorderPaint() {
        if (null != mTimeBgBorderPaint) return;

        mTimeBgBorderPaint = new Paint();
        mTimeBgBorderPaint.setAntiAlias(true);
        mTimeBgBorderPaint.setColor(mTimeBgBorderColor);
        if (!isDrawBg) {
            mTimeBgBorderPaint.setStrokeWidth(mTimeBgBorderSize);
            mTimeBgBorderPaint.setStyle(Paint.Style.STROKE_STYLE);
        }
    }

    private void initTimeTextBgDivisionLinePaint() {
        if (null != mTimeBgDivisionLinePaint) return;

        mTimeBgDivisionLinePaint = new Paint();
        mTimeBgDivisionLinePaint.setAntiAlias(true);
        mTimeBgDivisionLinePaint.setColor(mTimeBgDivisionLineColor);
        mTimeBgDivisionLinePaint.setStrokeWidth(mTimeBgDivisionLineSize);
    }

    @Override
    protected void initTimeTextBaseInfo() {
        super.initTimeTextBaseInfo();

        if (mDefSetTimeBgSize == 0 || mTimeBgSize < mTimeTextWidth) {
            mTimeBgSize = mTimeTextWidth + (Utils.dp2px(mContext, 2) * 4);
        }
    }

    /**
     * initialize time initialize rectF
     * @param topPaddingSize 顶部内间距
     */
    private void initTimeBgRect(float topPaddingSize) {
        float mHourLeft;
        float mMinuteLeft;
        float mSecondLeft;
        boolean isInitHasBackgroundTextBaseY = false;

        if (isShowDay) {
            // initialize day background and border rectF
            if (isShowTimeBgBorder) {
                mDayBgBorderRectF = new RectFloat(mLeftPaddingSize, topPaddingSize, mLeftPaddingSize + mDayTimeBgWidth + (mTimeBgBorderSize * 2), topPaddingSize + mTimeBgSize + (mTimeBgBorderSize * 2));
                mDayBgRectF = new RectFloat(mLeftPaddingSize + mTimeBgBorderSize, topPaddingSize + mTimeBgBorderSize, mLeftPaddingSize + mDayTimeBgWidth + mTimeBgBorderSize, topPaddingSize + mTimeBgSize + mTimeBgBorderSize);
            } else {
                mDayBgRectF = new RectFloat(mLeftPaddingSize, topPaddingSize, mLeftPaddingSize + mDayTimeBgWidth, topPaddingSize + mTimeBgSize);
            }
            // hour left point
            mHourLeft = mLeftPaddingSize + mDayTimeBgWidth + mSuffixDayTextWidth + mSuffixDayLeftMargin + mSuffixDayRightMargin + (mTimeBgBorderSize * 2);

            if (!isShowHour && !isShowMinute && !isShowSecond) {
                isInitHasBackgroundTextBaseY = true;
                initHasBackgroundTextBaseY(mDayBgRectF);
            }
        } else {
            // hour left point
            mHourLeft = mLeftPaddingSize;
        }

        if (isShowHour) {
            // initialize hour background border rectF
            if (isShowTimeBgBorder) {
                mHourBgBorderRectF = new RectFloat(mHourLeft, topPaddingSize, mHourLeft + mTimeBgSize + (mTimeBgBorderSize * 2), topPaddingSize + mTimeBgSize + (mTimeBgBorderSize * 2));
                mHourBgRectF = new RectFloat(mHourLeft + mTimeBgBorderSize, topPaddingSize + mTimeBgBorderSize, mHourLeft + mTimeBgSize + mTimeBgBorderSize, topPaddingSize + mTimeBgSize + mTimeBgBorderSize);
            } else {
                mHourBgRectF = new RectFloat(mHourLeft, topPaddingSize, mHourLeft + mTimeBgSize, topPaddingSize + mTimeBgSize);
            }
            // minute left point
            mMinuteLeft = mHourLeft + mTimeBgSize + mSuffixHourTextWidth + mSuffixHourLeftMargin + mSuffixHourRightMargin + (mTimeBgBorderSize * 2);

            if (!isInitHasBackgroundTextBaseY) {
                isInitHasBackgroundTextBaseY = true;
                initHasBackgroundTextBaseY(mHourBgRectF);
            }
        } else {
            // minute left point
            mMinuteLeft = mHourLeft;
        }

        if (isShowMinute) {
            // initialize minute background border rectF
            if (isShowTimeBgBorder) {
                mMinuteBgBorderRectF = new RectFloat(mMinuteLeft, topPaddingSize, mMinuteLeft + mTimeBgSize + (mTimeBgBorderSize * 2), topPaddingSize + mTimeBgSize + (mTimeBgBorderSize * 2));
                mMinuteBgRectF = new RectFloat(mMinuteLeft + mTimeBgBorderSize, topPaddingSize + mTimeBgBorderSize, mMinuteLeft + mTimeBgSize + mTimeBgBorderSize, topPaddingSize + mTimeBgSize + mTimeBgBorderSize);
            } else {
                mMinuteBgRectF = new RectFloat(mMinuteLeft, topPaddingSize, mMinuteLeft + mTimeBgSize, topPaddingSize + mTimeBgSize);
            }
            // second left point
            mSecondLeft = mMinuteLeft + mTimeBgSize + mSuffixMinuteTextWidth + mSuffixMinuteLeftMargin + mSuffixMinuteRightMargin + (mTimeBgBorderSize * 2);

            if (!isInitHasBackgroundTextBaseY) {
                isInitHasBackgroundTextBaseY = true;
                initHasBackgroundTextBaseY(mMinuteBgRectF);
            }
        } else {
            // second left point
            mSecondLeft = mMinuteLeft;
        }

        if (isShowSecond) {
            // initialize second background border rectF
            if (isShowTimeBgBorder) {
                mSecondBgBorderRectF = new RectFloat(mSecondLeft, topPaddingSize, mSecondLeft + mTimeBgSize + (mTimeBgBorderSize * 2), topPaddingSize + mTimeBgSize + (mTimeBgBorderSize * 2));
                mSecondBgRectF = new RectFloat(mSecondLeft + mTimeBgBorderSize, topPaddingSize + mTimeBgBorderSize, mSecondLeft + mTimeBgSize + mTimeBgBorderSize, topPaddingSize + mTimeBgSize + mTimeBgBorderSize);
            } else {
                mSecondBgRectF = new RectFloat(mSecondLeft, topPaddingSize, mSecondLeft + mTimeBgSize, topPaddingSize + mTimeBgSize);
            }

            if (isShowMillisecond) {
                // millisecond left point
                float mMillisecondLeft = mSecondLeft + mTimeBgSize + mSuffixSecondTextWidth + mSuffixSecondLeftMargin + mSuffixSecondRightMargin + (mTimeBgBorderSize * 2);

                // initialize millisecond background border rectF
                if (isShowTimeBgBorder) {
                    mMillisecondBgBorderRectF = new RectFloat(mMillisecondLeft, topPaddingSize, mMillisecondLeft + mTimeBgSize + (mTimeBgBorderSize * 2), topPaddingSize + mTimeBgSize + (mTimeBgBorderSize * 2));
                    mMillisecondBgRectF = new RectFloat(mMillisecondLeft + mTimeBgBorderSize, topPaddingSize + mTimeBgBorderSize, mMillisecondLeft + mTimeBgSize + mTimeBgBorderSize, topPaddingSize + mTimeBgSize + mTimeBgBorderSize);
                } else {
                    mMillisecondBgRectF = new RectFloat(mMillisecondLeft, topPaddingSize, mMillisecondLeft + mTimeBgSize, topPaddingSize + mTimeBgSize);
                }
            }

            if (!isInitHasBackgroundTextBaseY) {
                initHasBackgroundTextBaseY(mSecondBgRectF);
            }
        }
    }

    private float getSuffixTextBaseLine(String suffixText, float topPaddingSize) {
//        Rect tempRect = new Rect();
        Rect tempRect = mSuffixTextPaint.getTextBounds(suffixText);

        float ret;
        switch (mSuffixGravity) {
            case 0:
                // top
                ret = topPaddingSize - tempRect.top;
                break;
            default:
            case 1:
                // center
                ret = topPaddingSize + mTimeBgSize - mTimeBgSize / 2 + tempRect.getHeight() / 2 + mTimeBgBorderSize;
                break;
            case 2:
                // bottom
                ret = topPaddingSize + mTimeBgSize - tempRect.bottom + (mTimeBgBorderSize * 2);
                break;
        }

        return ret;
    }

    private void initHasBackgroundTextBaseY(RectFloat rectF) {
        // time text baseline
        Paint.FontMetrics timeFontMetrics = mTimeTextPaint.getFontMetrics();
        mTimeTextBaseY = rectF.top + (rectF.bottom - rectF.top - timeFontMetrics.bottom + timeFontMetrics.top) / 2 - timeFontMetrics.top - mTimeTextBottom;
        // initialize background division line y point
        mTimeBgDivisionLineYPos = rectF.getCenter().getPointY() + (mTimeBgDivisionLineSize == Utils.dp2px(mContext, DEFAULT_TIME_BG_DIVISION_LINE_SIZE) ? mTimeBgDivisionLineSize : mTimeBgDivisionLineSize / 2);
    }

    /**
     * initialize time text baseline
     * and
     * time background top padding
     * @param viewHeight  控件高度
     * @param viewPaddingTop 控件顶部间距
     * @param viewPaddingBottom 控件底部间距
     * @param contentAllHeight 控件内容高度
     * @return 中心分割线间距
     */
    private float initTimeTextBaselineAndTimeBgTopPadding(int viewHeight, int viewPaddingTop, int viewPaddingBottom, int contentAllHeight) {
        float topPaddingSize;
        if (viewPaddingTop == viewPaddingBottom) {
            // center
            topPaddingSize = (viewHeight - contentAllHeight) / 2;
        } else {
            // padding top
            topPaddingSize = viewPaddingTop;
        }

        if (isShowDay && mSuffixDayTextWidth > 0) {
            mSuffixDayTextBaseline = getSuffixTextBaseLine(mSuffixDay, topPaddingSize);
        }

        if (isShowHour && mSuffixHourTextWidth > 0) {
            mSuffixHourTextBaseline = getSuffixTextBaseLine(mSuffixHour, topPaddingSize);
        }

        if (isShowMinute && mSuffixMinuteTextWidth > 0) {
            mSuffixMinuteTextBaseline = getSuffixTextBaseLine(mSuffixMinute, topPaddingSize);
        }

        if (mSuffixSecondTextWidth > 0) {
            mSuffixSecondTextBaseline = getSuffixTextBaseLine(mSuffixSecond, topPaddingSize);
        }

        if (isShowMillisecond && mSuffixMillisecondTextWidth > 0) {
            mSuffixMillisecondTextBaseline = getSuffixTextBaseLine(mSuffixMillisecond, topPaddingSize);
        }

        return topPaddingSize;
    }

    @Override
    public int getAllContentWidth() {
        float width = getAllContentWidthBase(mTimeBgSize + (mTimeBgBorderSize * 2));

        if (isShowDay) {
            if (isDayLargeNinetyNine) {
//                Rect rect = new Rect();
                String tempDay = String.valueOf(mDay);
                Rect rect = mTimeTextPaint.getTextBounds(tempDay);
                mDayTimeBgWidth = rect.getWidth() + (Utils.dp2px(mContext, 2) * 4);
                width += mDayTimeBgWidth;
            } else {
                mDayTimeBgWidth = mTimeBgSize;
                width += mTimeBgSize;
            }

            width += (mTimeBgBorderSize * 2);
        }

        return (int) Math.ceil(width);
    }

    @Override
    public int getAllContentHeight() {
        return (int) (mTimeBgSize + (mTimeBgBorderSize * 2));
    }

    @Override
    public void onMeasure(Component v, int viewWidth, int viewHeight, int allContentWidth, int allContentHeight) {
        float retTopPaddingSize = initTimeTextBaselineAndTimeBgTopPadding(viewHeight, v.getPaddingTop(), v.getPaddingBottom(), allContentHeight);
        mLeftPaddingSize = v.getPaddingLeft() == v.getPaddingRight() ? (viewWidth - allContentWidth) / 2 : v.getPaddingLeft();
        initTimeBgRect(retTopPaddingSize);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        // show background
        float mHourLeft;
        float mMinuteLeft;
        float mSecondLeft;

        if (isShowDay) {
            // draw day background border
            if (isShowTimeBgBorder) {
                canvas.drawRoundRect(mDayBgBorderRectF, mTimeBgBorderRadius, mTimeBgBorderRadius, mTimeBgBorderPaint);
            }
            if (isDrawBg) {
                // draw day background
                canvas.drawRoundRect(mDayBgRectF, mTimeBgRadius, mTimeBgRadius, mTimeBgPaint);
                if (isShowTimeBgDivisionLine) {
                    // draw day background division line
                    canvas.drawLine(new Point(mLeftPaddingSize + mTimeBgBorderSize, mTimeBgDivisionLineYPos), new Point(mLeftPaddingSize + mDayTimeBgWidth + mTimeBgBorderSize, mTimeBgDivisionLineYPos), mTimeBgDivisionLinePaint);

                }
            }
            // draw day text
            canvas.drawText(mTimeTextPaint, Utils.formatNum(mDay), mDayBgRectF.getCenter().getPointX(), mTimeTextBaseY);
            if (mSuffixDayTextWidth > 0) {
                // draw day suffix
                canvas.drawText(mSuffixTextPaint, mSuffixDay, mLeftPaddingSize + mDayTimeBgWidth + mSuffixDayLeftMargin + (mTimeBgBorderSize * 2), mSuffixDayTextBaseline);
            }

            // hour left point
            mHourLeft = mLeftPaddingSize + mDayTimeBgWidth + mSuffixDayTextWidth + mSuffixDayLeftMargin + mSuffixDayRightMargin + (mTimeBgBorderSize * 2);
        } else {
            // hour left point
            mHourLeft = mLeftPaddingSize;
        }

        if (isShowHour) {
            // draw hour background border
            if (isShowTimeBgBorder) {
                canvas.drawRoundRect(mHourBgBorderRectF, mTimeBgBorderRadius, mTimeBgBorderRadius, mTimeBgBorderPaint);
            }
            if (isDrawBg) {
                // draw hour background
                canvas.drawRoundRect(mHourBgRectF, mTimeBgRadius, mTimeBgRadius, mTimeBgPaint);
                if (isShowTimeBgDivisionLine) {
                    // draw hour background division line
                    canvas.drawLine(new Point(mHourLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), new Point(mTimeBgSize + mHourLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), mTimeBgDivisionLinePaint);

                }
            }
            // draw hour text
            canvas.drawText(mTimeTextPaint, Utils.formatNum(mHour), mHourBgRectF.getCenter().getPointX(), mTimeTextBaseY);
            if (mSuffixHourTextWidth > 0) {
                // draw hour suffix
                canvas.drawText(mSuffixTextPaint, mSuffixHour, mHourLeft + mTimeBgSize + mSuffixHourLeftMargin + (mTimeBgBorderSize * 2), mSuffixHourTextBaseline);
            }

            // minute left point
            mMinuteLeft = mHourLeft + mTimeBgSize + mSuffixHourTextWidth + mSuffixHourLeftMargin + mSuffixHourRightMargin + (mTimeBgBorderSize * 2);
        } else {
            // minute left point
            mMinuteLeft = mHourLeft;
        }

        if (isShowMinute) {
            // draw minute background border
            if (isShowTimeBgBorder) {
                canvas.drawRoundRect(mMinuteBgBorderRectF, mTimeBgBorderRadius, mTimeBgBorderRadius, mTimeBgBorderPaint);
            }
            if (isDrawBg) {
                // draw minute background
                canvas.drawRoundRect(mMinuteBgRectF, mTimeBgRadius, mTimeBgRadius, mTimeBgPaint);
                if (isShowTimeBgDivisionLine) {
                    // draw minute background division line
                    canvas.drawLine(new Point(mMinuteLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), new Point(mTimeBgSize + mMinuteLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), mTimeBgDivisionLinePaint);
                }
            }
            // draw minute text
            canvas.drawText(mTimeTextPaint, Utils.formatNum(mMinute), mMinuteBgRectF.getCenter().getPointX(), mTimeTextBaseY);
            if (mSuffixMinuteTextWidth > 0) {
                // draw minute suffix
                canvas.drawText(mSuffixTextPaint, mSuffixMinute, mMinuteLeft + mTimeBgSize + mSuffixMinuteLeftMargin + (mTimeBgBorderSize * 2), mSuffixMinuteTextBaseline);
            }

            // second left point
            mSecondLeft = mMinuteLeft + mTimeBgSize + mSuffixMinuteTextWidth + mSuffixMinuteLeftMargin + mSuffixMinuteRightMargin + (mTimeBgBorderSize * 2);
        } else {
            // second left point
            mSecondLeft = mMinuteLeft;
        }

        if (isShowSecond) {
            // draw second background border
            if (isShowTimeBgBorder) {
                canvas.drawRoundRect(mSecondBgBorderRectF, mTimeBgBorderRadius, mTimeBgBorderRadius, mTimeBgBorderPaint);
            }
            if (isDrawBg) {
                // draw second background
                canvas.drawRoundRect(mSecondBgRectF, mTimeBgRadius, mTimeBgRadius, mTimeBgPaint);
                if (isShowTimeBgDivisionLine) {
                    // draw second background division line
                    canvas.drawLine(new Point(mSecondLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), new Point(mTimeBgSize + mSecondLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), mTimeBgDivisionLinePaint);
                }
            }
            // draw second text
            canvas.drawText(mTimeTextPaint, Utils.formatNum(mSecond), mSecondBgRectF.getCenter().getPointX(), mTimeTextBaseY);
            if (mSuffixSecondTextWidth > 0) {
                // draw second suffix
                canvas.drawText(mSuffixTextPaint, mSuffixSecond, mSecondLeft + mTimeBgSize + mSuffixSecondLeftMargin + (mTimeBgBorderSize * 2), mSuffixSecondTextBaseline);
            }

            if (isShowMillisecond) {
                // draw millisecond background border
                if (isShowTimeBgBorder) {
                    canvas.drawRoundRect(mMillisecondBgBorderRectF, mTimeBgBorderRadius, mTimeBgBorderRadius, mTimeBgBorderPaint);
                }
                // millisecond left point
                float mMillisecondLeft = mSecondLeft + mTimeBgSize + mSuffixSecondTextWidth + mSuffixSecondLeftMargin + mSuffixSecondRightMargin + (mTimeBgBorderSize * 2);
                if (isDrawBg) {
                    // draw millisecond background
                    canvas.drawRoundRect(mMillisecondBgRectF, mTimeBgRadius, mTimeBgRadius, mTimeBgPaint);
                    if (isShowTimeBgDivisionLine) {
                        // draw millisecond background division line
                        canvas.drawLine(new Point(mMillisecondLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), new Point(mTimeBgSize + mMillisecondLeft + mTimeBgBorderSize, mTimeBgDivisionLineYPos), mTimeBgDivisionLinePaint);
                    }
                }
                // draw millisecond text
                canvas.drawText(mTimeTextPaint, Utils.formatMillisecond(mMillisecond), mMillisecondBgRectF.getCenter().getPointX(), mTimeTextBaseY);
                if (mSuffixMillisecondTextWidth > 0) {
                    // draw millisecond suffix
                    canvas.drawText(mSuffixTextPaint, mSuffixMillisecond, mMillisecondLeft + mTimeBgSize + mSuffixMillisecondLeftMargin + (mTimeBgBorderSize * 2), mSuffixMillisecondTextBaseline);
                }
            }
        }
    }

    public void setTimeBgSize(float size) {
        mTimeBgSize = Utils.dp2px(mContext, size);
    }


    public void setTimeBgColor(Color color) {
        mTimeBgColor = color;
        mTimeBgPaint.setColor(mTimeBgColor);
        if (color == Color.TRANSPARENT && isShowTimeBgBorder) {
            isDrawBg = false;
            mTimeBgBorderPaint.setStrokeWidth(mTimeBgBorderSize);
            mTimeBgBorderPaint.setStyle(Paint.Style.STROKE_STYLE);
        } else {
            isDrawBg = true;
            if (isShowTimeBgBorder) {
                mTimeBgBorderPaint.setStrokeWidth(0);
                mTimeBgBorderPaint.setStyle(Paint.Style.FILL_STYLE);
            }
        }
    }

    public void setTimeBgRadius(float radius) {
        mTimeBgRadius = Utils.dp2px(mContext, radius);
    }

    public void setIsShowTimeBgDivisionLine(boolean isShow) {
        isShowTimeBgDivisionLine = isShow;
        if (isShowTimeBgDivisionLine) {
            initTimeTextBgDivisionLinePaint();
        } else {
            mTimeBgDivisionLinePaint = null;
        }
    }

    public void setTimeBgDivisionLineColor(Color color) {
        mTimeBgDivisionLineColor = color;
        if (null != mTimeBgDivisionLinePaint) {
            mTimeBgDivisionLinePaint.setColor(mTimeBgDivisionLineColor);
        }
    }

    public void setTimeBgDivisionLineSize(float size) {
        mTimeBgDivisionLineSize = Utils.dp2px(mContext, size);
        if (null != mTimeBgDivisionLinePaint) {
            mTimeBgDivisionLinePaint.setStrokeWidth(mTimeBgDivisionLineSize);
        }
    }

    public void setIsShowTimeBgBorder(boolean isShow) {
        isShowTimeBgBorder = isShow;
        if (isShowTimeBgBorder) {
            initTimeBgBorderPaint();
        } else {
            mTimeBgBorderPaint = null;
            mTimeBgBorderSize = 0;
        }
    }

    public void setTimeBgBorderColor(Color color) {
        mTimeBgBorderColor = color;
        if (null != mTimeBgBorderPaint) {
            mTimeBgBorderPaint.setColor(mTimeBgBorderColor);
        }
    }

    public void setTimeBgBorderSize(float size) {
        mTimeBgBorderSize = Utils.dp2px(mContext, size);
        if (null != mTimeBgBorderPaint && !isDrawBg) {
            mTimeBgBorderPaint.setStrokeWidth(mTimeBgBorderSize);
            mTimeBgBorderPaint.setStyle(Paint.Style.STROKE_STYLE);
        }
    }

    public void setTimeBgBorderRadius(float size) {
        mTimeBgBorderRadius = Utils.dp2px(mContext, size);
    }

}

