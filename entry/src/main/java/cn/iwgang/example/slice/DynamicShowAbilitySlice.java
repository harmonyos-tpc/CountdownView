/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.iwgang.example.slice;


import cn.iwgang.countdownview.DynamicConfig;

import cn.iwgang.countdownview.CountdownView;

import cn.iwgang.example.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;

/**
 * 属性设置页面具体实现类
 */
public class DynamicShowAbilitySlice extends AbilitySlice {
    private boolean isShowDay = true;
    private boolean isShowHour = true;
    private boolean isShowMinute = true;
    private boolean isShowSecond = true;
    private boolean isShowMillisecond = true;
    private CountdownView mCvCountdownViewTest;
    private CountdownView mCvCountdownViewTestHasBg;
    private DirectionalLayout mLlBackgroundConfigContainer;
    private DirectionalLayout mLlConvertDaysToHoursContainer;

    private final long TIME = (long) 8 * 24 * 60 * 60 * 1000;
    private boolean isConvertDaysToHours = false;
    private boolean hasBackgroundCountdownView = false;
    private float timeTextSize = 22;
    private float suffixTextSize = 12;
    private float timeBgSize = 40;
    private float bgRadius = 3;
    private float bgBorderSize;
    private float bgBorderRadius;
    private DirectionalLayout mLlConvertTimeConfigContentContainer;
    private DirectionalLayout mLlConvertTimeConfigTitleContainer;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_dynamic_show);

        initUI();
    }

    private void initUI() {
        Checkbox cbDay = (Checkbox) findComponentById(ResourceTable.Id_cb_day);
        Checkbox cbHour = (Checkbox) findComponentById(ResourceTable.Id_cb_hour);
        Checkbox cbMinute = (Checkbox) findComponentById(ResourceTable.Id_cb_minute);
        Checkbox cbSecond = (Checkbox) findComponentById(ResourceTable.Id_cb_second);
        Checkbox cbMillisecond = (Checkbox) findComponentById(ResourceTable.Id_cb_millisecond);
        cbDay.setChecked(true);
        cbHour.setChecked(true);
        cbMinute.setChecked(true);
        cbSecond.setChecked(true);
        cbMillisecond.setChecked(true);
        checkStateChangeListener(cbDay, cbHour, cbMinute, cbSecond, cbMillisecond);


        mCvCountdownViewTest = (CountdownView) findComponentById(ResourceTable.Id_cv_countdownViewTest);
        mCvCountdownViewTestHasBg = (CountdownView) findComponentById(ResourceTable.Id_cv_countdownViewTestHasBg);
        mLlBackgroundConfigContainer = (DirectionalLayout) findComponentById(ResourceTable.Id_ll_backgroundConfigContainer);
        mLlConvertDaysToHoursContainer = (DirectionalLayout) findComponentById(ResourceTable.Id_ll_convertDaysToHoursContainer);
        mLlConvertTimeConfigTitleContainer = (DirectionalLayout) findComponentById(ResourceTable.Id_timeconfigtitle);
        mLlConvertTimeConfigContentContainer = (DirectionalLayout) findComponentById(ResourceTable.Id_timeconfigcontent);

        mCvCountdownViewTest.start(TIME);
        initClickListener();
    }

    private void checkStateChangeListener(Checkbox cbDay, Checkbox cbHour, Checkbox cbMinute, Checkbox cbSecond, Checkbox cbMillisecond) {
        cbDay.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b1) {
                isShowDay = b1;
            }
        });

        cbHour.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b1) {
                isShowHour = b1;
            }
        });
        cbMinute.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b1) {
                isShowMinute = b1;
            }
        });
        cbSecond.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b1) {
                isShowSecond = b1;
                if (!b1 &&isShowMillisecond) {
                    cbMillisecond.setChecked(false);
                }
            }
        });
        cbMillisecond.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b1) {
                isShowMillisecond = b1;
                if (b1 && !isShowSecond) {
                    cbSecond.setChecked(true);
                }
            }
        });
    }

    private void initClickListener() {
        findComponentById(ResourceTable.Id_btn_theme1).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                if (hasBackgroundCountdownView) {
                    DynamicConfig.BackgroundInfo backgroundInfo = new DynamicConfig.BackgroundInfo();
                    backgroundInfo.setColor(0xFFFF54BC)
                            .setSize(30f)
                            .setRadius(0f)
                            .setShowTimeBgDivisionLine(false);
                    dynamicConfigBuilder.setTimeTextSize(15)
                            .setTimeTextColor(0xFFFFFFFF)
                            .setTimeTextBold(true)
                            .setSuffixTextColor(0xFF000000)
                            .setSuffixTextSize(15)
                            .setBackgroundInfo(backgroundInfo)
                            .setShowDay(false).setShowHour(true).setShowMinute(true).setShowSecond(true).setShowMillisecond(true);
                } else {
                    dynamicConfigBuilder.setTimeTextSize(35)
                            .setTimeTextColor(0xFFFF5000)
                            .setTimeTextBold(true)
                            .setSuffixTextColor(0xFFFF5000)
                            .setSuffixTextSize(30)
                            .setSuffixTextBold(false)
                            .setSuffix(":")
                            .setSuffixMillisecond("") // Remove millisecond suffix
                            .setSuffixGravity(DynamicConfig.SuffixGravity.CENTER)
                            .setShowDay(false).setShowHour(false).setShowMinute(true).setShowSecond(true).setShowMillisecond(true);
                }
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_theme2).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                if (hasBackgroundCountdownView) {
                    DynamicConfig.BackgroundInfo backgroundInfo = new DynamicConfig.BackgroundInfo();
                    backgroundInfo.setColor(0xFFFF5000)
                            .setSize(60f)
                            .setRadius(30f)
                            .setShowTimeBgDivisionLine(false);
                    dynamicConfigBuilder.setTimeTextSize(42)
                            .setTimeTextColor(0xFFFFFFFF)
                            .setTimeTextBold(true)
                            .setSuffixTextColor(0xFF000000)
                            .setSuffixTextSize(42)
                            .setSuffixTextBold(true)
                            .setBackgroundInfo(backgroundInfo)
                            .setShowDay(false).setShowHour(true).setShowMinute(true).setShowSecond(true).setShowMillisecond(false);
                } else {
                    dynamicConfigBuilder.setTimeTextSize(60)
                            .setTimeTextColor(0xFF444444)
                            .setTimeTextBold(false)
                            .setSuffixTextColor(0xFF444444)
                            .setSuffixTextSize(20)
                            .setSuffixTextBold(false)
                            .setSuffixMinute("m")
                            .setSuffixMinuteLeftMargin(5)
                            .setSuffixMinuteRightMargin(10)
                            .setSuffixSecond("s")
                            .setSuffixSecondLeftMargin(5)
                            .setSuffixGravity(DynamicConfig.SuffixGravity.BOTTOM)
                            .setShowDay(false).setShowHour(false).setShowMinute(true).setShowSecond(true).setShowMillisecond(false);
                }
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_theme3).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                if (hasBackgroundCountdownView) {
                    DynamicConfig.BackgroundInfo backgroundInfo = new DynamicConfig.BackgroundInfo();
                    backgroundInfo.setColor(0xFF444444)
                            .setSize(40f)
                            .setRadius(3f)
                            .setShowTimeBgDivisionLine(true)
                            .setDivisionLineColor(Color.getIntColor("#30FFFFFF"))
                            .setDivisionLineSize(1f);
                    dynamicConfigBuilder.setTimeTextSize(22)
                            .setTimeTextColor(0xFFFFFFFF)
                            .setTimeTextBold(true)
                            .setSuffixTextColor(0xFF000000)
                            .setSuffixTextSize(18)
                            .setSuffixTextBold(true)
                            .setBackgroundInfo(backgroundInfo)
                            .setShowDay(true).setShowHour(true).setShowMinute(true).setShowSecond(true).setShowMillisecond(true);
                } else {
                    dynamicConfigBuilder.setTimeTextSize(22)
                            .setTimeTextColor(0xFF000000)
                            .setTimeTextBold(false)
                            .setSuffixTextColor(0xFF000000)
                            .setSuffixTextSize(12)
                            .setSuffixTextBold(false)
                            .setSuffixDay("天").setSuffixHour("小时").setSuffixMinute("分钟").setSuffixSecond("秒").setSuffixMillisecond("毫秒")
                            .setSuffixGravity(DynamicConfig.SuffixGravity.TOP)
                            .setShowDay(true).setShowHour(true).setShowMinute(true).setShowSecond(true).setShowMillisecond(true);
                }
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_timeTextSizePlus).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setTimeTextSize(++timeTextSize);
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_timeTextSizeSubtract).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (timeTextSize == 0){
                    return;
                }
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setTimeTextSize(--timeTextSize);
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_modTimeTextColor).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // 设置一个颜色
                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setTimeTextColor(0xffff0000).build();
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfig);
                }
            }
        });
        findComponentById(ResourceTable.Id_btn_modTimeTextColor1).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                // 设置一个颜色
                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setTimeTextColor(0xff00ff00).build();
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfig);
                }
            }
        });
        findComponentById(ResourceTable.Id_btn_modTimeTextColor2).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                // 设置一个颜色
                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setTimeTextColor(0xff0000ff).build();
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfig);
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_suffixTextSizePlus).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setSuffixTextSize(++suffixTextSize);
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                }
            }
        });


        findComponentById(ResourceTable.Id_btn_suffixTextSizeSubtract).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (timeTextSize == 0){
                    return;
                }
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setSuffixTextSize(--suffixTextSize);
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_modSuffixTextColor).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setSuffixTextColor(0xffff0000).build();
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfig);
                }
            }
        });
        findComponentById(ResourceTable.Id_btn_modSuffixTextColor1).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setSuffixTextColor(0xff00ff00).build();
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfig);
                }
            }
        });
        findComponentById(ResourceTable.Id_btn_modSuffixTextColor2).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setSuffixTextColor(0xff0000ff).build();
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfig);
                }
            }
        });

        ((Button)findComponentById(ResourceTable.Id_btn_suffixGravityTop)).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setSuffixGravity(DynamicConfig.SuffixGravity.TOP).build();
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfig);
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_suffixGravityCenter).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setSuffixGravity(DynamicConfig.SuffixGravity.CENTER).build();
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfig);
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_suffixGravityBottom).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setSuffixGravity(DynamicConfig.SuffixGravity.BOTTOM).build();
                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfig);
                }
            }
        });

        findComponentById(ResourceTable.Id_btn_refTimeShow).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!isShowDay && !isShowHour && !isShowMinute && !isShowSecond && !isShowMillisecond) {
                    ToastDialog toastDialog = new ToastDialog(getContext());
                    toastDialog.setText("Select at least one item").setDuration(2000).show();
                    return;
                }
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setShowDay(isShowDay)
                        .setShowHour(isShowHour)
                        .setShowMinute(isShowMinute)
                        .setShowSecond(isShowSecond)
                        .setShowMillisecond(isShowMillisecond);

                if (hasBackgroundCountdownView) {
                    mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
                } else {
                    mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
                }
            }
        });


        findComponentById(ResourceTable.Id_btn_bgSizePlus).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setSize(++timeBgSize));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        findComponentById(ResourceTable.Id_btn_bgSizeSubtract).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (timeBgSize == 0){
                    return;
                }
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setSize(--timeBgSize));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        findComponentById(ResourceTable.Id_btn_modBgColor).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setBackgroundInfo(new DynamicConfig.BackgroundInfo().setColor(0xff00ff00)).build();
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
            }
        });

        findComponentById(ResourceTable.Id_btn_modBgColor1).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig dynamicConfig = new DynamicConfig.Builder().setBackgroundInfo(new DynamicConfig.BackgroundInfo().setColor(0xff0000ff)).build();
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfig);
            }
        });
        findComponentById(ResourceTable.Id_btn_bgRadiuvplus).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setRadius(++bgRadius));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        findComponentById(ResourceTable.Id_btn_bgRadiusSubtract).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (bgRadius == 0){
                    return;
                }
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setRadius(--bgRadius));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });


        Button btnModBgDivisionLineColor = (Button) findComponentById(ResourceTable.Id_btn_modBgDivisionLineColor);
        btnModBgDivisionLineColor.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgDivisionLine(true).setDivisionLineColor(0xffff0000));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });
        Button btnModBgDivisionLineColor1 = (Button) findComponentById(ResourceTable.Id_btn_modBgDivisionLineColor1);
        btnModBgDivisionLineColor1.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgDivisionLine(true).setDivisionLineColor(0xff0000ff));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        Checkbox checkboxbgDividionLine = (Checkbox) findComponentById(ResourceTable.Id_cb_bgDivisionLine);
        checkboxbgDividionLine.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b1) {
                btnModBgDivisionLineColor.setEnabled(b1);
                btnModBgDivisionLineColor1.setEnabled(b1);
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgDivisionLine(b1));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        Button btnBgBorderSizePlus = (Button) findComponentById(ResourceTable.Id_btn_bgBorderSizePlus);
        Button btnBgBorderSizeSubtract = (Button) findComponentById(ResourceTable.Id_btn_bgBorderSizeSubtract);
        Button btnBgBorderRadiusPlus = (Button) findComponentById(ResourceTable.Id_btn_bgBorderRadiuvplus);
        Button btnBgBorderRadiusSubtract = (Button) findComponentById(ResourceTable.Id_btn_bgBorderRadiusSubtract);

        btnBgBorderSizePlus.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgBorder(true).setBorderSize(++bgBorderSize));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        btnBgBorderSizeSubtract.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (bgBorderSize == 0){
                    return;
                }
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgBorder(true).setBorderSize(--bgBorderSize));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        btnBgBorderRadiusPlus.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgBorder(true).setBorderRadius(++bgBorderRadius));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        btnBgBorderRadiusSubtract.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (bgBorderRadius == 0){
                    return;
                }
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgBorder(true).setBorderRadius(--bgBorderRadius));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        Button btnModBgBorderColor = (Button) findComponentById(ResourceTable.Id_btn_modBgBorderColor);

        btnModBgBorderColor.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgBorder(true).setBorderColor(0xffff0000));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        Button btnModBgBorderColor1 = (Button) findComponentById(ResourceTable.Id_btn_modBgBorderColor1);

        btnModBgBorderColor1.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgBorder(true).setBorderColor(0xffffff00));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });

        ((Checkbox)findComponentById(ResourceTable.Id_cb_bgBorder)).setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b1) {
                btnModBgBorderColor.setEnabled(b1);
                btnModBgBorderColor1.setEnabled(b1);
                btnBgBorderSizePlus.setEnabled(b1);
                btnBgBorderSizeSubtract.setEnabled(b1);
                btnBgBorderRadiusPlus.setEnabled(b1);
                btnBgBorderRadiusSubtract.setEnabled(b1);

                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setBackgroundInfo(new DynamicConfig.BackgroundInfo().setShowTimeBgBorder(b1));
                mCvCountdownViewTestHasBg.dynamicShow(dynamicConfigBuilder.build());
            }
        });
        ((Checkbox)findComponentById(ResourceTable.Id_cb_bgBorder)).setChecked(false);

        TextField etSuffixDay = (TextField) findComponentById(ResourceTable.Id_et_suffixDay);
        TextField etSuffixHour = (TextField) findComponentById(ResourceTable.Id_et_suffixHour);
        TextField etSuffixMinute = (TextField) findComponentById(ResourceTable.Id_et_suffixMinute);
        TextField etSuffixSecond = (TextField) findComponentById(ResourceTable.Id_et_suffixSecond);
        TextField etSuffixMillisecond = (TextField) findComponentById(ResourceTable.Id_et_suffixMillisecond);

        findComponentById(ResourceTable.Id_btm_refSuffix).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setSuffixDay(etSuffixDay.getText().toString())
                        .setSuffixHour(etSuffixHour.getText().toString())
                        .setSuffixMinute(etSuffixMinute.getText().toString())
                        .setSuffixSecond(etSuffixSecond.getText().toString())
                        .setSuffixMillisecond(etSuffixMillisecond.getText().toString());
                mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
            }
        });


        ((Checkbox)findComponentById(ResourceTable.Id_cb_hasBackground)).setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b1) {
                hasBackgroundCountdownView = b1;
                mLlBackgroundConfigContainer.setVisibility(b1 ? Component.VISIBLE : Component.HIDE);
                mCvCountdownViewTest.setVisibility(!b1 ? Component.VISIBLE : Component.HIDE);
                mCvCountdownViewTestHasBg.setVisibility(b1 ? Component.VISIBLE : Component.HIDE);

                if (b1) {
                    mCvCountdownViewTest.stop();
                    mCvCountdownViewTestHasBg.start(TIME);
                    mLlConvertDaysToHoursContainer.setVisibility(Component.HIDE);
                    mLlConvertTimeConfigContentContainer.setVisibility(Component.HIDE);
                    mLlConvertTimeConfigTitleContainer.setVisibility(Component.HIDE);

                } else {
                    mCvCountdownViewTestHasBg.stop();
                    mCvCountdownViewTest.start(TIME);
                    mLlConvertDaysToHoursContainer.setVisibility(Component.VISIBLE);
                    mLlConvertTimeConfigContentContainer.setVisibility(Component.VISIBLE);
                    mLlConvertTimeConfigTitleContainer.setVisibility(Component.VISIBLE);
                }
            }
        });

        Button btnConvertDaysToHours = (Button) findComponentById(ResourceTable.Id_btn_convertDaysToHours);

        findComponentById(ResourceTable.Id_btn_convertDaysToHours).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                isConvertDaysToHours = !isConvertDaysToHours;
                btnConvertDaysToHours.setText(isConvertDaysToHours ? "转换" : "不转换");

                DynamicConfig.Builder dynamicConfigBuilder = new DynamicConfig.Builder();
                dynamicConfigBuilder.setConvertDaysToHours(isConvertDaysToHours);
                dynamicConfigBuilder.setShowDay(!isConvertDaysToHours);
                mCvCountdownViewTest.dynamicShow(dynamicConfigBuilder.build());
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
